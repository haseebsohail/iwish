import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';

import Movie from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/AntDesign';

export default class Card extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        console.log(this.props.movie.Ratings[0].Value)
        return (
            <View style={styles.CardContainer} >
                <View>
                    {
                        this.props.movie.Poster === "N/A" ?
                            <Image
                                source={{ uri: 'https://thumbs.dreamstime.com/b/movie-theater-row-red-seats-clapperboard-soda-popcorn-ticket-cinema-premiere-poster-design-98021151.jpg' }}
                                style={{ width: 150, height: 150, margin: 5 }}
                            />
                            :
                            <View>
                                <Image
                                    source={{ uri: this.props.movie.Poster }}
                                    style={{ width: 150, height: 150, margin: 5 }}
                                />
                            </View>
                    }
                </View>
                <View style={{ marginTop: 10, marginRight: 15, flex: 1, flexDirection: "column" }}>
                    <Text style={{ fontSize: 12 }}>{this.props.movie.Title}</Text>
                    <Text style={{ fontSize: 12 }}>{this.props.movie.Year}</Text>
                    <Text style={{ fontSize: 12 }}>{this.props.movie.Released}</Text>
                    <Text style={{ fontSize: 12 }}>{this.props.movie.Runtime}</Text>
                    <View style={{ flexDirection: "row", marginTop: 5, }}>
                        <Icon name="star" size={20} color="#FFDF00" style={{ marginBottom: 10 }} />
                        <Icon name="star" size={20} color="#FFDF00" style={{ marginBottom: 10 }} />
                        <Icon name="star" size={20} color="#FFDF00" style={{ marginBottom: 10 }} />
                        <Icon name="star" size={20} color="#FFDF00" style={{ marginBottom: 10 }} />
                        <Icon name="star" size={20} color="#FFDF00" style={{ marginBottom: 10 }} />
                        <Text style={{ fontSize: 12, paddingLeft: 5 }}>{this.props.movie.Ratings[0].Value}</Text>
                    </View>

                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    CardContainer: {
        flexDirection: "row",
        width: "95%",
        margin: 15,
        backgroundColor: "white",
        borderRadius: 10
    }
});