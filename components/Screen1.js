//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { StyleSheet, View, Text, TextInput } from 'react-native';
// import all basic components
import Search from 'react-native-vector-icons/EvilIcons';
// import Colors from '../variables/CommonColors'
import Color from '../variables/CommonColor'

//import Card Component 
import Card from './Card'
export default class Screen1 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      movie: "",
      data: [],
      show: false,
    }
  }

  handleMovie = (text) => {

    if (this.state.text === "") {
      this.setState({
        show: false
      })
    }
    this.setState({ movie: text })
  }


  SearchMovie = () => {

    console.log(this.state.movie)

    fetch("http://www.omdbapi.com/?t='" + this.state.movie + "'&apikey=ca910cfc")
      .then(data => data.json())
      .then(res => {
        //  console.log(res);
        this.setState({
          data: res,
          show: true
        })
      })

  }

  render() {

    return (
      <View style={styles.MainContainer}>
        <View style={styles.search}>
          <TextInput style={styles.search_input}
            placeholder="Search"
            underlineColorAndroid="transparent"
            onChangeText={this.handleMovie}>
          </TextInput>

          <Search name="search" size={25} color={Color.HeaderColor} style={styles.search_bar} onPress={() => { this.SearchMovie() }} />

        </View>
        {

          this.state.show === true && this.state.data.Response !== "False" ? <Card movie={this.state.data}></Card> :


            this.state.data.Response === "False" ?
              <View style={{ margin: 10 }}>
                <Text>There is no movie related to search</Text>
              </View>
              : null

        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: 10,
    alignItems: 'center',
  },
  search: {
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    marginTop: 15,
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 5,
    height: 40,
    // justifyContent: "space-between"
  },
  search_bar: {
    marginLeft: 10,
    marginTop: 10,
    marginRight: 10,
  },
  search_input: {
    paddingLeft: 10,
    flex: 1,
  },
});